#pragma once
#ifndef _GOLD_GAMES_GRAPHICS_UTIL_H_
#define _GOLD_GAMES_GRAPHICS_UTIL_H_

#include <SFML\Graphics.hpp>
#include <Box2D\Box2D.h>

namespace gold {
  namespace games {
    class GraphicsUtil
    {
    private:
      float metersPerPixelRatio;
      float pixelsPerMeterRatio;

      static const float pi;

    public:
      explicit GraphicsUtil(float metersPerPixelRatio);
      ~GraphicsUtil();

      float getMetersPerPixelRatio() const;
      float getPixelsPerMeterRatio() const;

      sf::Vector2f transformBox2DToScreen(float x, float y) const;
      sf::Vector2f transformBox2DToScreen(const b2Vec2& vector) const;
      
      b2Vec2 transformScreenToBox2D(float x, float y) const;
      b2Vec2 transformScreenToBox2D(const sf::Vector2f& vector) const;

      static sf::Color getColor(const b2Color& colour, int alpha = 255);

      static float toRadians(float angleInDegrees);
      static float toDegrees(float angleInRadians);

      static const GraphicsUtil& getDefault();
    };
  }
}

#endif // _GOLD_GAMES_GRAPHICS_UTIL_H_