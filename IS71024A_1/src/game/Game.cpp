#include "Game.hpp"

#include <math.h>
#include <stdlib.h>
#include <algorithm>

#include <SFML\Graphics.hpp>
#include <Box2D\Box2D.h>

#include "..\debug\SFMLDebugDraw.hpp"

#include "GraphicsUtil.hpp"
#include "GameResourceManager.hpp"

namespace {

  using gold::games::Game;
  using gold::games::GameState;
  using gold::games::GameResourceManager;

  namespace util {
    
    const gold::games::GraphicsUtil& graphics = gold::games::GraphicsUtil::getDefault();

    float toRadians(float degrees) {
      return graphics.toRadians(degrees);
    }

    float toDegrees(float radians) {
      return graphics.toDegrees(radians);
    }

    sf::Vector2f transformBox2DToScreen(float x, float y) {
      return graphics.transformBox2DToScreen(x, y);
    }

    sf::Vector2f transformBox2DToScreen(const b2Vec2& vector) {
      return graphics.transformBox2DToScreen(vector);
    }
    
    b2Vec2 transformScreenToBox2D(float x, float y) {
      return graphics.transformScreenToBox2D(x, y);
    }
    
    b2Vec2 transformScreenToBox2D(const sf::Vector2f& vector) {
      return graphics.transformScreenToBox2D(vector);
    }
    
    void resize(sf::Sprite& sprite, const sf::Vector2u& originalSize, const sf::Vector2f& newSize) {
        float scaleX = newSize.x / originalSize.x;
        float scaleY = newSize.y / originalSize.y;
        sprite.scale(scaleX, scaleY);
    }
    
    template <typename T>
    T clamp(const T& value, const T& lowerBound, const T& upperBound) {
      return std::min(std::max(value, lowerBound), upperBound);
    }

    // Utility class which aggregates a Texture and its Sprite
    class Image {
    private:
      sf::Texture* _texture;
      sf::Sprite _sprite;

      // Non copyable entity
      Image(const Image&);
      Image operator=(const Image&);

    public:
      Image() {
      }

      Image(const char* resource) {
        loadFromFile(resource);
      }

      ~Image() {
      }
    
      bool loadFromFile(const char* resource) {
        _texture = GameResourceManager::getInstance().loadTexture(resource);
        if (_texture != NULL) {
          _sprite.setTexture(*_texture);
        }

        return _texture != NULL;
      }

      sf::Texture* getTexture() {
        return _texture;
      }

      sf::Sprite& getSprite() {
        return _sprite;
      }

      sf::Vector2u getSize() {
        return (_texture != NULL ? _texture->getSize() : sf::Vector2u(0, 0));
      }

      sf::Vector2f getPosition() {
        return _sprite.getPosition();
      }

      void setPosition(const sf::Vector2f& position) {
        _sprite.setPosition(position);
      }
      
      void setPosition(float x, float y) {
        _sprite.setPosition(x, y);
      }

      void rotate(float angleInDegrees) {
        _sprite.rotate(angleInDegrees);
      }

      void draw(sf::RenderTarget& canvas) const {
        canvas.draw(_sprite);
      }
    };

  }

  // -- Start

  class StartState : public GameState {
  public:
    StartState(Game* game) :
      GameState(game) {
    };

    ~StartState() {
    };

    GameState* operator()() {
      return getGame()->getState(Game::title);
    };
  };

  // -- Terminate

  class EndState : public GameState {
  public:
    EndState(Game* game) :
      GameState(game) {
    };

    ~EndState() {
    };

    GameState* operator()() {
      getGame()->getWindow().close();
      return this;
    };
  };

  // -- Title

  class Title : public GameState {
  private:
    util::Image _backgroundImage;
    util::Image _winImage;
    int _win;

    void renderTitle(sf::RenderTarget& canvas) {
      
      _backgroundImage.draw(canvas);

      // Maximum of 3 win 'trophies'
      for (int i = 0; i < std::min(_win, 3); ++i) {
        // Here we use the current view just in case it is scaled/zoomed out
        float xPos = canvas.getView().getSize().x - ((5 + _winImage.getSize().x) * (i + 1));
        float yPos = canvas.getView().getSize().y - 5 - _winImage.getSize().y;

        _winImage.setPosition(xPos, yPos);
        _winImage.draw(canvas);
      }

    }

  public:
    Title(Game* game) :
      GameState(game),
      _backgroundImage("splash.png"),
      _winImage("flower.png"),
      _win(0) {
        // NOTE the use of sf::Font and sf::Text is ideal but with the
        // font we would like to use, text fill is not available via the
        // SFML API. Text is now part of background image.
    };

    ~Title() {
    };

    void addWin() {
      ++_win;
    };

    GameState* operator()() {
      sf::RenderWindow& window = getGame()->getWindow();
      
      sf::Event event;
      while (window.isOpen()) {
        
        window.setTitle(Game::getDefaultWindowTitle());

        // SFML window event polling loop
        while (window.pollEvent(event)) {

          // Close Window Event
          if (event.type == sf::Event::Closed) {
            return getGame()->getState(Game::end);
          } else if (event.type == sf::Event::KeyPressed) {
            if (event.key.code == sf::Keyboard::Escape) {
              return getGame()->getState(Game::end);
            }
          } else if (event.type == sf::Event::MouseButtonPressed) {
            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
              return getGame()->getState(Game::level1);
            }
          }

        }

        // Reset window display
        window.clear();

        renderTitle(window);

        // Committ drawings to window display
        window.display();
      }

      return this;
    };
  };

  // -- Gameplay

  /**
   * Contains common methodology between
   * Level implementations
   */
  class Level : public GameState {
  protected:

    typedef struct {
      util::Image background;
      util::Image land;
      util::Image cloud;
      util::Image playerDroplet;
      util::Image droplet;
      util::Image flower;
    } graphics_t;

  private:
    // NOTE According to our design, pause and debug drawing
    // are specified and set *per* level and are required to
    // be set per level accordingly.
    bool _pause;
    
#ifdef _DEBUG
    gold::games::SFMLDebugDraw _debugDraw;
#endif
    
    graphics_t _graphics;

    sf::Vector2f _playerPosition;

  protected:
    
    // -- Typedefs

    typedef std::unique_ptr<b2World> b2World_ptr_t;

    // -- Constants
    
    static const float32 getTimeStep() {
      return 1.0f / 60.0f;
    }

    static const int getVelocityIterations() {
      // As recommended by Box2D Manual
      return 8;
    }
    
    static const int getPositionIterations() {
      // As recommended by Box2D Manual
      return 3;
    }

    static const b2Vec2& getGravity() {
      static b2Vec2 gravity(0.f, -9.8f);
      return gravity;
    }
    
    static const float32 getDropletRadius() {
      return 10.f;
    }

    static const sf::Vector2f& getDefaultPlayerPosition() {
      static sf::Vector2f position(8, 17);
      return position;
    }

    // -- Definitions

    typedef struct {

      b2World_ptr_t world;

      b2Body* playerDroplet;
      std::vector<b2Body*> droplets;

      virtual void reset() {
        world.reset();
        playerDroplet = NULL;
        droplets.clear();
      }

    } physics_base_t;

    // A tag structure which is used to tag
    // droplets which are player-created droplets.
    class PlayerTag {
    private:
      PlayerTag() { };

    public:
      ~PlayerTag() { };

      static PlayerTag* getInstance() {
        static PlayerTag _tag;
        return &_tag;
      };
    };
    
    // -- Methods

    void setPaused(bool pause) {
      _pause = pause;
    };

    void togglePaused() {
      _pause = !_pause;
    };
    
#ifdef _DEBUG
    bool isDebugDrawEnabled() {
      return _debugDraw.getRenderTarget() != NULL;
    };
    
    void setDebugDrawEnabled(bool enableDebugDraw) {
      _debugDraw.setRenderTarget((enableDebugDraw ? &(getGame()->getWindow()) : NULL));
    };
    
    void toggleDebugDraw() {
      setDebugDrawEnabled(!isDebugDrawEnabled());
    };

    gold::games::SFMLDebugDraw* getDebugDraw() {
      return &_debugDraw;
    };
#endif
    
    void tagAsPlayer(b2Body* body) {
      if (body != NULL) {
        body->SetUserData(PlayerTag::getInstance());
      }
    };

    bool isPlayer(const b2Body* body) {
      return body != NULL && body->GetUserData() != NULL && body->GetUserData() == PlayerTag::getInstance();
    };

    const sf::Vector2f& getPlayerPosition() const {
      return _playerPosition;
    };

    sf::Vector2f& getPlayerPosition() {
      return _playerPosition;
    };

    void setPlayerPosition(const sf::Vector2f& position) {
      _playerPosition = position;
    };
    
    void setPlayerPosition(float xPos) {
      static const float margin = 8.f;
      /*static*/ const float halfWidth = getGraphics().cloud.getSize().x / 2.f;
      /*static*/ const float offset = margin + halfWidth;

      getPlayerPosition().x = util::clamp(xPos, offset, getWindowSize().x - offset) - halfWidth;
    };
    
    sf::Vector2f getPlayerDropletPosition(const sf::Vector2f& playerPosition) {
      return sf::Vector2f(
          (playerPosition.x + (getGraphics().cloud.getSize().x / 2.f) - 6),
          140.f - getDropletRadius()
        );
    };
    
    sf::Vector2f getPlayerDropletPosition() {
      return getPlayerDropletPosition(getPlayerPosition());
    };
    
    b2Vec2 getb2PlayerDropletPosition(const sf::Vector2f& dropletPosition) {
      return util::transformScreenToBox2D(
          (dropletPosition.x + getDropletRadius()),
          (dropletPosition.y + getGraphics().droplet.getSize().y - getDropletRadius())
        );
    };

    b2Vec2 getb2PlayerDropletPosition() {
      return getb2PlayerDropletPosition(getPlayerDropletPosition());
    };
    
    graphics_t& getGraphics() {
      return _graphics;
    };

    void initialiseGraphics() {
      _graphics.background.loadFromFile("sky.png");
      _graphics.cloud.loadFromFile("cloud.png");
      _graphics.playerDroplet.loadFromFile("playerDroplet.png");
      _graphics.droplet.loadFromFile("droplet.png");
      _graphics.flower.loadFromFile("flower.png");
      _graphics.land.loadFromFile("land.png");
    };

    virtual GameState* handleEvent(const sf::Event& event) {
      // If the window close button is pressed
      if (event.type == sf::Event::Closed) {
        return getGame()->getState(Game::end);
      } else if (event.type == sf::Event::KeyPressed) {
        // 'esc' key -> Return to title screen
        if (event.key.code == sf::Keyboard::Escape) {
          reset();
          return getGame()->getState(Game::title);
        // 'p' key -> Pause game
        } else if (event.key.code == sf::Keyboard::P) {
          togglePaused();
#ifdef _DEBUG
        // '~' key -> Toggle Box2D debug draw
        } else if (event.key.code == sf::Keyboard::Tilde) {
          toggleDebugDraw();
        // '1' key -> Skip to Level 1
        } else if (event.key.code == sf::Keyboard::Num1) {
          reset();
          return getGame()->getState(Game::level1);
        // '2' key -> Skip to Level 2
        } else if (event.key.code == sf::Keyboard::Num2) {
          reset();
          return getGame()->getState(Game::level2);
#endif
        }
      }

      return NULL;
    };

    void addTrophy() {
      // Add trophy to title screen
      Title *title = (Title*) getGame()->getState(Game::title);
      title->addWin();
    };
    
    void renderImage(util::Image& image, const sf::Vector2f& position, float angle, sf::RenderTarget& canvas) {
      sf::Vector2f oldSpritePosition = image.getPosition();
      
      image.setPosition(position);
      image.rotate(angle);
      image.draw(canvas);
      image.setPosition(oldSpritePosition);
      image.rotate(-angle);
    };
    
    void renderCloud(const sf::Vector2f& position, sf::RenderTarget& canvas) {
      renderImage(getGraphics().cloud, position, 0.f, canvas);
    };

    virtual void renderDroplet(const b2Body* droplet, sf::RenderTarget& canvas) {
      sf::Vector2f position = util::transformBox2DToScreen(droplet->GetPosition());
      
      // Offset sprite accordingly
      position.x -= getDropletRadius();
      position.y -= getDropletRadius() + 14;
      
      // Avoid rendering out-of-bounds droplets
      // Fixes the issue were droplets were being rendered on the top-left corner of the window
      if (!(position.x > 0 && position.x < getWindowSize().x &&
        position.y > 0 && position.y < getWindowSize().y)) {
          return;
      }

      util::Image& image = (isPlayer(droplet) ? getGraphics().playerDroplet : getGraphics().droplet);

      // Droplet will sway left to right at a maximum of 10 degrees
      float angle = sin(droplet->GetAngle()) * -10;

      renderImage(image, position, angle, canvas);
    };

    void renderDroplets(const physics_base_t& physics, sf::RenderTarget& canvas) {
      for (size_t i = 0; i < physics.droplets.size(); ++i) {
        renderDroplet(physics.droplets[i], canvas);
      }

      if (physics.playerDroplet != NULL) {
        renderDroplet(physics.playerDroplet, canvas);
      }
    };

    void renderPlayer(const sf::Vector2f& position, bool renderDroplet, sf::RenderTarget& canvas) {
      renderCloud(position, canvas);

      if (renderDroplet) {
        renderImage(getGraphics().playerDroplet, getPlayerDropletPosition(), 0.f, canvas);
      }
    }

    void renderPlayer(bool renderDroplet, sf::RenderTarget& canvas) {
      renderPlayer(getPlayerPosition(), renderDroplet, canvas);
    }

    sf::Vector2f getMousePosition() {
      sf::RenderWindow& window = getGame()->getWindow();
      return window.mapPixelToCoords(sf::Mouse::getPosition(window));
    };
     
    void updatePlayerPosition() {
      setPlayerPosition(getMousePosition().x);
    };
    
  public:
    Level(Game* game) :
      GameState(game) {
        initialiseGraphics();
    };

    const sf::Vector2f& getWindowSize() {
      // We are using default view in order to return
      // the default video mode size (800x600).
      return getGame()->getWindow().getDefaultView().getSize();
    };

    bool isPaused() const {
      return _pause;
    };

    virtual void reset() {
      setPaused(false);
      setPlayerPosition(getDefaultPlayerPosition());
    };

  };

  // -- Level 1

  class Level1 : public Level {
  private:

    // -- Constants

    static const int maxDropCount = 25;

    // -- Definitions

    class ContactListener : public b2ContactListener {
    private:
      Level1* _outerType;
      std::vector<b2Body*> _groundHits;
      std::vector<b2Body*> _dropletHits;

    public:
      explicit ContactListener(Level1* outerType = NULL) :
        _outerType(outerType) {
      };

      virtual ~ContactListener() {
      };

      void clear() {
        _groundHits.clear();
        _dropletHits.clear();
      };

      void reset(Level1* outerType) {
        _outerType = outerType;
        clear();
      }

      Level1* getOuterType() {
        return _outerType;
      }

      std::vector<b2Body*>& getGroundHits() {
        return _groundHits;
      }
      
      std::vector<b2Body*>& getDropletHits() {
        return _dropletHits;
      }

      void checkDropletGroundContact(b2Contact* contact) {
        b2Body* droplet = NULL;

        if (contact->GetFixtureA()->GetBody() == _outerType->physics.ground) {
          droplet = contact->GetFixtureB()->GetBody();
        } else if (contact->GetFixtureB()->GetBody() == _outerType->physics.ground) {
          droplet = contact->GetFixtureA()->GetBody();
        }

        if (droplet != NULL) {
          // Record the droplet
          _groundHits.push_back(droplet);
        }
      };

      void checkDropletDropletContact(b2Contact* contact) {
        b2Body* droplet = NULL;
        b2Body* other = NULL;

        // Avoid droplet search by using the assumption that droplets of interest
        // are specified as kinematic
        if (contact->GetFixtureA()->GetBody()->GetType() == b2_kinematicBody) {
          droplet = contact->GetFixtureA()->GetBody();
          other = contact->GetFixtureB()->GetBody();
        } else if (contact->GetFixtureB()->GetBody()->GetType() == b2_kinematicBody) {
          droplet = contact->GetFixtureB()->GetBody();
          other = contact->GetFixtureA()->GetBody();
        }
        
        // Release the droplet from kinematic restraints if the kinematic droplet is not the player
        // itself and the collision occurs between a player droplet and other background droplet
        if (droplet != NULL && droplet != _outerType->physics.playerDroplet && _outerType->isPlayer(other)) {
          // For now, simply keep a record of the contact. Reassigning body types during simulation
          // leads to asserts in the Box2D code.
          _dropletHits.push_back(droplet);
        }
      };

      // -- b2ContactListener Implementation

      virtual void BeginContact(b2Contact* contact) {
        checkDropletGroundContact(contact);
        checkDropletDropletContact(contact);
      };

      virtual void EndContact(b2Contact* contact) { };
      virtual void PreSolve(b2Contact* contact, const b2Manifold*oldManifold) { };
      virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) { };
    };

    // -- Class Variables

    typedef struct : physics_base_t {
      ContactListener listener;

      b2Body* ground;
      std::vector<b2Body*> flowers;

      virtual void reset() {
        physics_base_t::reset();

        listener.clear();
        ground = NULL;
        flowers.clear();
      }

    } physics_t;

    physics_t physics;
    
#ifdef _AUDIO
    sf::Sound dropletSound;
#endif

    int dropCount;

    // -- Methods

    static const float32 getFlowerRadius() {
      return 23.f;
    }

    b2Body* initialiseDroplet(const b2Vec2& position, float32 angularVelocity = 0.2f) {
      
      b2BodyDef dropletBodyDef;
      dropletBodyDef.type = b2_kinematicBody;
      dropletBodyDef.position = position;
      dropletBodyDef.angularVelocity = angularVelocity;

      b2Body* dropletBody = physics.world->CreateBody(&dropletBodyDef);

      b2CircleShape dropletShape;
      dropletShape.m_radius = getDropletRadius();
      
      b2FixtureDef dropletFixture;
      dropletFixture.shape = &dropletShape;
      dropletFixture.density = 1.0f;
      dropletFixture.restitution = 0.3f;

      dropletBody->CreateFixture(&dropletFixture);

      return dropletBody;

    }

    b2Body* initialiseFlower(const b2Vec2& position) {

      b2BodyDef dropletBodyDef;
      dropletBodyDef.type = b2_staticBody;
      dropletBodyDef.position = position;

      b2Body* flowerBody = physics.world->CreateBody(&dropletBodyDef);

      b2CircleShape flowerShape;
      flowerShape.m_radius = getFlowerRadius();
      
      b2FixtureDef flowerFixture;
      flowerFixture.shape = &flowerShape;
      flowerFixture.density = 1.0f;

      flowerBody->CreateFixture(&flowerFixture);

      return flowerBody;

    }

    void initialiseWorld() {
      // Reset physics structure
      physics.reset();
      physics.listener.reset(this);

      // --

      // Allocating b2World on the heap since errors were
      // encountered when allocated on the stack and returned
      // by value. Return by value implies construction on stack,
      // copy operation on return and destruction of world stored
      // on stack. This lead to undefined body pointers in returned
      // value.
      b2World* world = new b2World(getGravity());

      // Specify contact listener
      world->SetContactListener(&physics.listener);
      
#ifdef _DEBUG
      world->SetDebugDraw(getDebugDraw());
#endif

      physics.world.reset(world);

      // --

      // 'Invisible' Ground
      b2BodyDef groundBodyDef;
      groundBodyDef.type = b2_staticBody;
      groundBodyDef.position = util::transformScreenToBox2D(400, 575);
      
      b2Body* groundBody = world->CreateBody(&groundBodyDef);
      
      b2PolygonShape groundBox;
      groundBox.SetAsBox(400.0f, 5.0f);

      b2Fixture *groundFixture = groundBody->CreateFixture(&groundBox, 0.0f);
      groundFixture->SetSensor(true);

      physics.ground = groundBody;

      // --

      // Target droplets
      int x, y = 0;
      for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j) {
          x = (i % 2 == 0 ? 100 : 215) + (j * 115);
          y = 230 + (i * 60);

          b2Body* droplet = initialiseDroplet(util::transformScreenToBox2D((float) x, (float) y));
          physics.droplets.push_back(droplet);
        }
      }
    }

    void simulate() {
      physics.world->Step(getTimeStep(), getVelocityIterations(), getPositionIterations());

      // NOTE Setting body type needs to occur after simluation. During Box2D world
      // simulation, the world gets locked, and changing body types during that phase
      // will result in asserts.
      for (size_t i = 0; i < physics.listener.getDropletHits().size(); ++i) {
        // Remove kinematic constraints from droplets
        physics.listener.getDropletHits()[i]->SetType(b2_dynamicBody);
        // Increment score
        ++dropCount;
      }

      // Generate flowers when droplets hit the ground
      for (size_t i = 0; i < physics.listener.getGroundHits().size(); ++i) {
        b2Body* droplet = physics.listener.getGroundHits()[i];
        physics.flowers.push_back(initialiseFlower(droplet->GetPosition()));

        std::remove(physics.droplets.begin(), physics.droplets.end(), droplet);
        if (droplet == physics.playerDroplet) {
          physics.playerDroplet = NULL;
        }

        physics.world->DestroyBody(droplet);
      }

      // A cleanup stage of out-of-bounds objects may
      // be beneficial at this point but expensive.
    }

    void playSounds() {
#ifdef _AUDIO
      for (size_t i = 0; i < physics.listener.getDropletHits().size(); ++i) {
        dropletSound.play();
      }
#endif
    }

    void renderPlayer(sf::RenderTarget& canvas) {
      Level::renderPlayer((physics.playerDroplet == NULL), canvas);
    }
    
    void renderDroplets(sf::RenderTarget& canvas) {
      Level::renderDroplets(physics, canvas);
    }

    void renderFlowers(sf::RenderTarget& canvas) {
      util::Image& flowerImage = getGraphics().flower;

      for (size_t i = 0; i < physics.flowers.size(); ++i) {
        b2Body* flower = physics.flowers[i];
        sf::Vector2f position = util::transformBox2DToScreen(flower->GetPosition());
        
        // Sprite offset
        position.x -= getFlowerRadius();
        position.y -= getFlowerRadius() + 15;

        flowerImage.getSprite().setPosition(position);
        flowerImage.draw(canvas);
      }
    }

    void render(sf::RenderTarget& canvas) {
      getGraphics().background.draw(canvas);
      getGraphics().land.draw(canvas);

      renderPlayer(canvas);
      renderDroplets(canvas);
      renderFlowers(canvas);

#ifdef _DEBUG
      if (isDebugDrawEnabled()) {
        physics.world->DrawDebugData();
      }
#endif
    }

    void createPlayerDroplet() {
      // Treat old player droplet as a fallen droplet
      physics.droplets.push_back(physics.playerDroplet);
      // Signal that a player droplet is to be created
      physics.playerDroplet = NULL;
    }

    GameState* handleEvent(const sf::Event& event) {
      sf::RenderWindow& window = getGame()->getWindow();
      
      // Keep a record of the current pause state
      bool pause = isPaused();

      GameState* state = NULL;
      if ((state = Level::handleEvent(event)) != NULL) {
        return state;
      }
      
      if (!isPaused()) {

        // In case the player unpauses the game, immediately
        // reflect mouse position on player object
        if (pause != isPaused()) {
          updatePlayerPosition();
        }

        if (event.type == sf::Event::MouseMoved) {
          // Move player representation according to mouse position
          updatePlayerPosition();
        } else if (event.type == sf::Event::MouseButtonPressed) {
          if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            // Release droplet
            if (physics.playerDroplet == NULL) {
              // Initialise physical representation of droplet at mouse position
              physics.playerDroplet = initialiseDroplet(getb2PlayerDropletPosition());
              physics.playerDroplet->SetType(b2_dynamicBody);
              tagAsPlayer(physics.playerDroplet);

              // To avoid droplet slinging...
              physics.playerDroplet->SetLinearVelocity(b2Vec2_zero);

              // Apply a random angular impulse so that droplets move (left or right) on contact
              physics.playerDroplet->ApplyAngularImpulse((float) ((rand() % 10) - 5));
            // Else create new droplet
            } else {
              createPlayerDroplet();
            }
          } else if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
            // Only create new player droplet if and only if the player droplet has been released
            if (physics.playerDroplet != NULL) {
              createPlayerDroplet();
            }
          }
        }
      }

      return NULL;
    }

    bool isWin() {
      return dropCount >= maxDropCount;
    }

  public:
    Level1(gold::games::Game* game) :
      Level(game) {
        float yPos = getWindowSize().y - getGraphics().land.getSize().y;
        getGraphics().land.setPosition(0, yPos);

#ifdef _AUDIO
        dropletSound.setBuffer(*GameResourceManager::getInstance().loadSoundBuffer("droplet.wav"));
#endif

        reset();
    };

    ~Level1() {
    };
    
    void reset() {
      Level::reset();

      dropCount = 0;
      initialiseWorld();
    }

    GameState* operator()() {
      sf::RenderWindow& window = getGame()->getWindow();
      GameState* state = NULL;

      window.setTitle(std::string(Game::getDefaultWindowTitle()) + " - Hit 'em all!");

      // On first entry, update player position immediately
      // to reflect current mouse position
      updatePlayerPosition();

      sf::Event event;
      while (window.isOpen()) {

        // Reset any droplet hits
        physics.listener.clear();

        // SFML window event polling loop
        while (window.pollEvent(event)) {
          if ((state = handleEvent(event)) != NULL) {
            return state;
          }
        }

        if (isWin()) {
          reset();
          addTrophy();
          return getGame()->getState(Game::level2);
        }

        if (!isPaused()) {
          simulate();
        }
        
        window.clear();
        render(window);
        window.display();

        playSounds();

      }

      return this;
    };
  };

  // -- Level 2

  class Level2 : public Level {
  private:

    typedef struct : physics_base_t {

      // Scale
      typedef struct {
        
        b2Body* rod;
        b2Body* rodHinge;

        b2Joint* rodJoint;
        
        // Plate
        typedef struct {
          b2Body* plate;
          b2Joint* plateRodJoint[2];
        } plate_t;

        // Left and right plate instatiation
        plate_t plates[2];

      } scale_t;

      scale_t scale;

      virtual void reset() {
        physics_base_t::reset();

        scale.rod = NULL;
        scale.rodHinge = NULL;
        scale.rodJoint = NULL;

        scale.plates[0].plate = NULL;
        scale.plates[0].plateRodJoint[0] = NULL;
        scale.plates[0].plateRodJoint[1] = NULL;
        
        scale.plates[1].plate = NULL;
        scale.plates[1].plateRodJoint[0] = NULL;
        scale.plates[1].plateRodJoint[1] = NULL;
      }

    } physics_t;

    physics_t physics;

    int balancePeriod;

    bool isBalanced() {
      float yDiff = physics.scale.plates[0].plate->GetPosition().y - physics.scale.plates[1].plate->GetPosition().y;
      return abs(yDiff) <= 10.f;
    }

    void simulate() {
      physics.world->Step(getTimeStep(), getVelocityIterations(), getPositionIterations());
      balancePeriod = (isBalanced() ? balancePeriod + 1 : 0);
    }

    using Level::renderCloud;

    void renderCloud(const b2Vec2& position, sf::RenderTarget& canvas) {
      sf::Vector2f newSpritePosition = util::transformBox2DToScreen(position);
      newSpritePosition.x -= getGraphics().cloud.getSize().x / 2.f;
      newSpritePosition.y -= getGraphics().cloud.getSize().y;

      renderCloud(newSpritePosition, canvas);
    }

    void renderPlayer(sf::RenderTarget& canvas) {
      Level::renderPlayer((physics.playerDroplet == NULL), canvas);
    }

    void renderDroplets(sf::RenderTarget& canvas) {
      Level::renderDroplets(physics, canvas);
    }

    const sf::Font& getFont() const {
	    return *GameResourceManager::getInstance().loadFont("font.ttf");
    }

    void renderBalanced(sf::RenderTarget& canvas) {
	    static sf::Text text(sf::String("OK"), getFont());
      text.setColor(sf::Color::Green);
	    text.setPosition(getWindowSize().x - 70, getWindowSize().y - 50);
		
	    canvas.draw(text);
    }

    void render(sf::RenderWindow& window) {
      getGraphics().background.draw(window);

      renderPlayer(window);

      renderCloud(physics.scale.plates[0].plate->GetPosition(), window);
      renderCloud(physics.scale.plates[1].plate->GetPosition(), window);

      renderDroplets(window);

	  if (isBalanced()) {
		  renderBalanced(window);
	  };

#ifdef _DEBUG
      if (isDebugDrawEnabled()) {
        physics.world->DrawDebugData();
      }
#endif
    }

    void initialisePlate(
      physics_t::scale_t& scale,
      physics_t::scale_t::plate_t& plate,
      const b2Vec2& position,
      const b2Vec2& scaleAnchor,
      b2World* world
    ) {

      const float32 platehx = 75.f;
      b2Vec2 plateOffset = position;

      b2BodyDef plateDef;
      plateDef.type = b2_dynamicBody;
      plateDef.position = position;
      plate.plate = world->CreateBody(&plateDef);

      b2PolygonShape plateShape;
      // 150 x 10
      plateShape.SetAsBox(platehx, 5.f);
      plate.plate->CreateFixture(&plateShape, 1.f);

      // -- Barriers
      
      // Left Barrier
      b2PolygonShape barrierShape;
      barrierShape.SetAsBox(5.f, 15.f, b2Vec2(-platehx, 7.f), 0.f);
      plate.plate->CreateFixture(&barrierShape, 1.f);

      // Right Barrier
      barrierShape.SetAsBox(5.f, 15.f, b2Vec2(platehx, 7.f), 0.f);
      plate.plate->CreateFixture(&barrierShape, 1.f);

      // -- Joints
      
      // -- Left Joint
      
      plateOffset.x = position.x - (platehx - 5);

      b2DistanceJointDef plateScaleLeftJointDef;
      plateScaleLeftJointDef.Initialize(scale.rod, plate.plate, scaleAnchor, plateOffset);
      plate.plateRodJoint[0] = world->CreateJoint(&plateScaleLeftJointDef);
      
      // -- Right Joint
      
      plateOffset.x = position.x + (platehx - 5);

      b2DistanceJointDef plateScaleRightJointDef;
      plateScaleRightJointDef.Initialize(scale.rod, plate.plate, scaleAnchor, plateOffset);
      plate.plateRodJoint[1] = world->CreateJoint(&plateScaleRightJointDef);

    }

    void initialiseScale(physics_t::scale_t& scale, b2World* world) {
      
      const float32 rodhx = 220.f;
      const b2Vec2 anchor = util::transformScreenToBox2D(400, 90);

      // --

      b2BodyDef rodDef;
      rodDef.type = b2_dynamicBody;
      rodDef.position = anchor;
      scale.rod = world->CreateBody(&rodDef);

      b2PolygonShape rodShape;
      // 500 x 10
      rodShape.SetAsBox(rodhx, 5.f);
      b2Fixture *rodFixture = scale.rod->CreateFixture(&rodShape, 1.f);
      rodFixture->SetSensor(true);

      // --
      
      b2BodyDef rodHingeDef;
      rodHingeDef.type = b2_staticBody;
      rodHingeDef.position = anchor;
      scale.rodHinge = world->CreateBody(&rodHingeDef);
      
      b2PolygonShape rodHingeShape;
      // 10 x 10
      rodHingeShape.SetAsBox(5.f, 5.f);
      b2Fixture *rodHingeFixture = scale.rodHinge->CreateFixture(&rodHingeShape, 1.f);
      rodHingeFixture->SetSensor(true);

      b2RevoluteJointDef rodHingeJointDef;
      rodHingeJointDef.Initialize(scale.rod, scale.rodHinge, anchor);
      rodHingeJointDef.lowerAngle = util::toRadians(-15.f);
      rodHingeJointDef.upperAngle = util::toRadians(+15.f);
      rodHingeJointDef.enableLimit = true;
      scale.rodJoint = world->CreateJoint(&rodHingeJointDef);

      // --

      b2Vec2 scaleAnchor = scale.rod->GetPosition();
      scaleAnchor.x -= rodhx - 5;

      // Left
      initialisePlate(
        scale,
        scale.plates[0],
        util::transformScreenToBox2D(scaleAnchor.x, 400),
        scaleAnchor,
        world
      );

      scaleAnchor.x = scale.rod->GetPosition().x + rodhx - 5;
      
      // Right
      initialisePlate(
        scale,
        scale.plates[1],
        util::transformScreenToBox2D(scaleAnchor.x, 400),
        scaleAnchor,
        world
      );

    }

    b2Body* initialiseDroplet(const b2Vec2& position, b2World* world) {
      b2BodyDef dropletBodyDef;
      dropletBodyDef.type = b2_dynamicBody;
      dropletBodyDef.position = position;
      
      b2Body* droplet = world->CreateBody(&dropletBodyDef);

      b2CircleShape circle;
      circle.m_radius = getDropletRadius();

      droplet->CreateFixture(&circle, 1.0f);

      return droplet;
    }

    void initialiseWorld() {
      
      physics.reset();

      // --

      b2World* world = new b2World(getGravity());
      
#ifdef _DEBUG
      world->SetDebugDraw(getDebugDraw());
#endif

      physics.world.reset(world);

      // --

      initialiseScale(physics.scale, world);

      // --

      b2Vec2 dropletPosition;

      // Initialise 5 droplets on the left plate
      dropletPosition.x = physics.scale.plates[0].plate->GetPosition().x - 60;
      dropletPosition.y = physics.scale.plates[0].plate->GetPosition().y + 10;
      for (int i = 0; i < 5; ++i) {
        dropletPosition.x += 20;
        physics.droplets.push_back(initialiseDroplet(dropletPosition, world));
      }

    }

    void releasePlayerDroplet() {
      physics.playerDroplet = initialiseDroplet(getb2PlayerDropletPosition(), physics.world.get());
      tagAsPlayer(physics.playerDroplet);
    }

    void createPlayerDroplet() {
      physics.droplets.push_back(physics.playerDroplet);
      physics.playerDroplet = NULL;
    }

    GameState* handleEvent(const sf::Event& event) {
      GameState* state = NULL;
      bool pause = isPaused();

      if ((state = Level::handleEvent(event)) != NULL) {
        return state;
      }

      if (!isPaused()) {
        if (pause != isPaused()) {
          updatePlayerPosition();
        }

        if (event.type == sf::Event::MouseMoved) {
          updatePlayerPosition();
        } else if (event.type == sf::Event::MouseButtonPressed) {
          if (event.mouseButton.button == sf::Mouse::Left) {
            if (physics.playerDroplet == NULL) {
              releasePlayerDroplet();
            } else {
              createPlayerDroplet();
            }
          } else if (event.mouseButton.button == sf::Mouse::Right) {
            if (physics.playerDroplet != NULL) {
              createPlayerDroplet();
            }
          }
        }
      }

      return NULL;
    }

    bool isWin() {
      // Scale is balanced for approximately x simulation steps
      return isBalanced() && balancePeriod >= 1200;
    }

  public:
    Level2(Game* game) :
      Level(game) {
      reset();
    }

    ~Level2() {
    }

    void reset() {
      Level::reset();

      balancePeriod = 0;
      initialiseWorld();
    }

    GameState* operator()() {
      GameState* state = NULL;
      sf::RenderWindow& window = getGame()->getWindow();

      window.setTitle(std::string(Game::getDefaultWindowTitle()) + " - Balance!");
      updatePlayerPosition();

      sf::Event event;
      while (window.isOpen()) {

        while (window.pollEvent(event)) {
          if ((state = handleEvent(event)) != NULL) {
            return state;
          }
        }

        if (isWin()) {
          reset();
          addTrophy();
          return getGame()->getState(Game::title);
        }

        if (!isPaused()) {
          simulate();
        }
        
        window.clear();
        render(window);
        window.display();

      }

      return this;
    };
  };

}

namespace gold {
  namespace games {

    GameState::GameState(Game* game) :
      _game(game) {
    }
    
    GameState::~GameState() {
    }

    Game* GameState::getGame() {
      return _game;
    }

    Game::Game(sf::RenderWindow& window) :
      _window(window) {
      _gameStates.push_back(new StartState(this));
      _gameStates.push_back(new Title(this));
      _gameStates.push_back(new Level1(this));
      _gameStates.push_back(new Level2(this));
      _gameStates.push_back(new EndState(this));

      _state = _gameStates[start];
    }

    Game::~Game() {
      for (size_t i = 0; i < _gameStates.size(); ++i) {
        delete _gameStates[i];
      }
    }
    
    sf::RenderWindow& Game::getWindow() const {
      return _window;
    }

    const char* Game::getDefaultWindowTitle() {
      static const char* title = "CloudMan [A Tools and Middleware Project]";
      return title;
    }

    GameState* Game::getState(State state) const {
      return _gameStates[state];
    }

    void Game::operator()() {
      _state = (*_state)();
    }

    bool Game::hasNext() const {
      return _window.isOpen();
    }

  }
}
