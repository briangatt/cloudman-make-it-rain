#include "SFMLDebugDraw.hpp"

#include <SFML\Graphics.hpp>

#include "..\game\GraphicsUtil.hpp"

namespace {

  const gold::games::GraphicsUtil& graphics = gold::games::GraphicsUtil::getDefault();
  const b2Color b2Black(0.f, 0.f, 0.f);
  const b2Color b2White(1.f, 1.f, 1.f);

  sf::Vector2f getVector(const b2Vec2& vector) {
    return graphics.transformBox2DToScreen(vector);
  }
  
  sf::Vector2f getVector(float x, float y) {
    return graphics.transformBox2DToScreen(x, y);
  }

  sf::Color getColor(const b2Color& color, int alpha = 150) {
    return graphics.getColor(color, alpha);
  }

  void _Draw(const sf::Drawable& drawable, sf::RenderTarget* canvas = NULL) {
    if (canvas != NULL) {
      canvas->draw(drawable);
    }
  }

  // Reference: https://github.com/slok/Box2D-and-SFML-demo/blob/master/src/DebugDraw.cpp

  // Share one instance of the Drawable object and modify accordingly per drawing
  sf::ConvexShape polygon;
  void _DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color, sf::RenderTarget* canvas, bool fill = false) {
    polygon.setPointCount(vertexCount);
    for (int32 i = 0; i < vertexCount; ++i) {
      polygon.setPoint(i, getVector(vertices[i]));
    }

    sf::Color colour = getColor(color);
    if (fill) {
      polygon.setFillColor(colour);
    } else {
      polygon.setOutlineColor(colour);
    }

    _Draw(polygon, canvas);
  }
  
  sf::VertexArray line(sf::Lines, 2);
  void _DrawLine(const b2Vec2& pointA, const b2Vec2& pointB, const b2Color& color, sf::RenderTarget* canvas) {
    sf::Color colour = getColor(color);
    line[0] = sf::Vertex(getVector(pointA), colour);
    line[1] = sf::Vertex(getVector(pointB), colour);
    
    _Draw(line, canvas);
  }
  
  sf::CircleShape circle;
  void _DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color, sf::RenderTarget* canvas, const b2Vec2* axis = NULL) {
    circle.setRadius(radius * graphics.getPixelsPerMeterRatio());
    // Because circle position in SFML is defined by the top-left corner
    circle.setPosition(getVector(center.x - radius, center.y + radius));
    
    sf::Color colour = getColor(color);
    if (axis == NULL) {
      circle.setOutlineColor(colour);
    } else {
      circle.setFillColor(colour);
    }
    
    _Draw(circle, canvas);
    if (axis != NULL) {
      // axis is a basis vector. Therefore scaling and translation is required.
      b2Vec2 north = center + (radius * (*axis));
      _DrawLine(center, north, b2Black, canvas);
    }
  }

}

namespace gold {
  namespace games {

    SFMLDebugDraw::SFMLDebugDraw(sf::RenderTarget* canvas) :
      canvas(canvas) {
        // By default, set shapes and joints to be drawn
        SetFlags(e_shapeBit | e_jointBit);
    }

    SFMLDebugDraw::~SFMLDebugDraw() {
    }

    sf::RenderTarget* SFMLDebugDraw::getRenderTarget() {
      return canvas;
    }
    
    void SFMLDebugDraw::setRenderTarget(sf::RenderTarget* canvas) {
      this->canvas = canvas;
    }

    void SFMLDebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) {
      _DrawPolygon(vertices, vertexCount, color, getRenderTarget());
    }

    void SFMLDebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) {
      _DrawPolygon(vertices, vertexCount, color, getRenderTarget(), true);
    }

    void SFMLDebugDraw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color) {
      _DrawCircle(center, radius, color, getRenderTarget());
    }

    void SFMLDebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color) {
      _DrawCircle(center, radius, color, getRenderTarget(), &axis);
    }

    void SFMLDebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) {
      _DrawLine(p1, p2, color, getRenderTarget());
    }

    void SFMLDebugDraw::DrawTransform(const b2Transform& xf) {
    }

  }
}