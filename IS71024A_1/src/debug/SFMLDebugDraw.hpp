#pragma once
#ifndef _GOLD_GAMES_SFML_DEBUG_DRAW_H_
#define _GOLD_GAMES_SFML_DEBUG_DRAW_H_

#include <Box2D\Box2D.h>

// Forward Declarations

namespace sf {
  class RenderTarget;
}

namespace gold {
  namespace games {
    /**
     * A b2Draw implementation specifically
     * for SFML
     */
    class SFMLDebugDraw : public b2Draw
    {
    private:
      sf::RenderTarget* canvas;

    public:
      SFMLDebugDraw(sf::RenderTarget* canvas = NULL);
      virtual ~SFMLDebugDraw();

      sf::RenderTarget* getRenderTarget();
      void setRenderTarget(sf::RenderTarget* canvas);

      // b2Draw Interface

      virtual void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
      virtual void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
      virtual void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
      virtual void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
      virtual void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
      virtual void DrawTransform(const b2Transform& xf);
    };
  }
}

#endif // _GOLD_GAMES_SFML_DEBUG_DRAW_H_