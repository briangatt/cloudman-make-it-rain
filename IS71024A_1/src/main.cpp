#include "game\Game.hpp"
#include "game\GameResourceManager.hpp"

#include <SFML\Graphics.hpp>

namespace {
  // SFML Constants
  const int screenWidth  = 800;
  const int screenHeight = 600;
  const int bpp = 32;

  const sf::VideoMode videoMode(screenWidth, screenHeight, bpp);
}

int main(int argc, char* argv[]) {
  sf::RenderWindow window(videoMode, gold::games::Game::getDefaultWindowTitle());

  // Set window icon
  sf::Image windowIcon;
  sf::Texture* texture = gold::games::GameResourceManager::getInstance().loadTexture("cloud.png");
  if (texture != NULL) {
    windowIcon = texture->copyToImage();
    window.setIcon(windowIcon.getSize().x, windowIcon.getSize().y, windowIcon.getPixelsPtr());
  }
  
  gold::games::Game game(window);
  while (game.hasNext()) {
    game();
  }

	return 0;
}